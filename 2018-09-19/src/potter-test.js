import {getOptimizedGroupedCartItems, getUniqueCartItemsCount, getCartGroupsGrandTotal} from './potter';

// Un panier [1, 1, 2, 3, 4, 5]

describe('Potter', () => {

    describe('getUniqueCartItemsCount', () => {
        it('should count unique cart item', () => {
            expect(getUniqueCartItemsCount([])).to.deep.equal({});
            expect(getUniqueCartItemsCount([1])).to.deep.equal({1: 1});
            expect(getUniqueCartItemsCount([1, 1])).to.deep.equal({1: 2});
            expect(getUniqueCartItemsCount([1, 2, 3])).to.deep.equal({1: 1, 2: 1, 3: 1});
        })
    });
    
    describe('getCartGroupsGrandTotal', () => {
        it('compute the groups total price', () => {
            expect(getCartGroupsGrandTotal([])).to.deep.equal(0);
            expect(getCartGroupsGrandTotal([[1]])).to.be.equal(8);
            expect(getCartGroupsGrandTotal([[1], [1]])).to.be.equal(16);
            expect(getCartGroupsGrandTotal([[1, 2, 3]])).to.be.equal(3 * 8 * 0.9);
            expect(getCartGroupsGrandTotal([[1, 2, 3], [2]])).to.be.equal(3 * 8 * 0.9 + 8);
            expect(getCartGroupsGrandTotal([[1, 2, 3, 4], [1, 2, 3, 5]])).to.be.equal(4 * 8 * 0.8 * 2);
        })
    });
    
    describe('getOptimizedGroupedCartItems', () => {
        it('compute the most optimized groups total price', () => {
            expect(getOptimizedGroupedCartItems([])).to.deep.equal([]);
            expect(getOptimizedGroupedCartItems([1])).to.deep.equal([[1]]);
            expect(getOptimizedGroupedCartItems([1, 1])).to.deep.equal([[1], [1]]);
            expect(getOptimizedGroupedCartItems([1, 2, 3])).to.deep.equal([[1, 2, 3]]);
            expect(getOptimizedGroupedCartItems([1, 2, 2, 3])).to.deep.equal([[1, 2, 3], [2]]);
            expect(getOptimizedGroupedCartItems([1, 1, 2, 2, 3, 3, 4, 5])).to.deep.equal([[1, 2, 3, 4], [1, 2, 3, 5]]);
            expect(getOptimizedGroupedCartItems([1, 1, 2, 2, 3, 3, 4, 5, 5])).to.deep.equal([[1, 2, 3, 4, 5], [1, 2, 3, 5]]);
            expect(getOptimizedGroupedCartItems([1, 1, 2, 2, 3, 3, 4, 5, 5, 5, 5])).to.deep.equal([[1, 2, 3, 4, 5], [1, 2, 3, 5], [5], [5]]);
        })
    })
});
