export const REGULAR_PRICE = 8;
export const DISCOUNTS = {
    1: 0,
    2: 5,
    3: 10,
    4: 20,
    5: 25
};

export function getUniqueCartItemsCount(products) {
    return products.reduce((acc, i) => {
        if (acc[i]) {
            ++acc[i];
        } else {
            acc[i] = 1;
        }
        return acc;
    }, {});
}

export function getOptimizedGroupedCartItems(products) {
    const counts = getUniqueCartItemsCount(products);
    let currentGroup = [];
    for (let bookNumber in counts) {
        
        const isBookNumberFive = (bookNumber === '5'), intBookNumber = 1 * bookNumber;
        
        if (isBookNumberFive) {
            for (let indexGroup in currentGroup) {
                
                if (counts[bookNumber] === 0) {
                    break;
                }
                
                if (currentGroup[indexGroup].length === 3) {
                    currentGroup[indexGroup].push(intBookNumber);
                    counts[bookNumber]--;
                }
            }
        }
        
        for (let i = 0, count = counts[bookNumber]; i < count; ++i) {
            if (!currentGroup[i]) {
                currentGroup[i] = [];
            }
            
            if (!isBookNumberFive || currentGroup[i].indexOf(intBookNumber) === -1) {
                currentGroup[i].push(intBookNumber);
            } else {
                count++;
            }
        }
    }
    
    return currentGroup;
}

export function getCartGroupsGrandTotal(groups) {
    return groups.reduce((acc, groupItems) => {
        let discount = 1 - DISCOUNTS[groupItems.length] / 100;
        
        return acc + REGULAR_PRICE * groupItems.length * discount;
    }, 0);
}

export function getGrandTotal(products) {
    return getCartGroupsGrandTotal(getOptimizedGroupedCartItems(products));
}

