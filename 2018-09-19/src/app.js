import { getUniqueCartItemsCount, getOptimizedGroupedCartItems, getGrandTotal } from './potter'

console.log('A super JS app !');
console.log(getUniqueCartItemsCount([1, 1, 1, 1, 2, 2, 3, 4, 5]));
console.log(getUniqueCartItemsCount([1, 2, 3, 4, 1, 2, 3, 5]));
console.log(getOptimizedGroupedCartItems([1, 1, 1, 1, 2, 2, 3, 4, 5]));
console.log(getOptimizedGroupedCartItems([1, 2, 3, 4, 1, 2, 3, 5]));
console.log(getGrandTotal([1, 1, 1, 1, 2, 2, 3, 4, 5]));
console.log(getGrandTotal([1, 2, 3, 4, 1, 2, 3, 5]));