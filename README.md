# Coding Dojo - Dev'In Rodez
Ce repos contient les codes que nous avons réalisés durant les soirées coding dojo organisées par l'Association.

## Soirée du 11/07/2018 - Liste des Katas réalisés :
+ [Anagram](./2018-07-11/anagram.php)
+ [FizzBuzz](./2018-07-11/fizzbuzz.php)
