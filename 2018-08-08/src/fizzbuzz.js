
export const isMultipleOf = (multiplier, value) => value % multiplier === 0;

export const filterBuilder = (multiplier, result) => ({
  multiplier,
  execute: value => (isMultipleOf(multiplier, value) ? result : value),
});

export class Converter {
  constructor(filters) {
    this.filters = filters.sort((a, b) => b.multiplier - a.multiplier);
  }

  getFilters() {
    return this.filters;
  }

  convert(value) {
    // TODO: must be refactored
    for (const filter of this.filters) {
      const newValue = filter.execute(value);
      if (newValue !== value) {
        return newValue;
      }
    }
    return value;
  }
}

