<?php

/**
 * Tri la chaîne de caractère par ordre alphabétique
 *
 * @param string $string
 * @return string La chaîne de caractère triée
 */
function str_sort(string $string): string
{
    $letters = str_split($string);
    sort($letters);

    return implode('', $letters);
}



define('HAYSTACK', str_sort('documenting'));
define('HAYSTACK_LENGTH', strlen(HAYSTACK));



// Lire le fichier
// Créer un tableau a partir des mots du fichier

$f = fopen(__DIR__ . '/anagram.txt', 'rb');
$pool = [];
while ($line = fgets($f)) {
    $pool = array_merge($pool, preg_split('@\s+@', trim($line)));
}
fclose($f);


// Trouver les anagrammes
// Nettoyage des mots non pertinents
$words_by_length = [];
foreach ($pool as $word) {
    $length = strlen($word);

    // Ignore les mot plus long ou de la même longueur
    if ($length >= HAYSTACK_LENGTH) {
        continue;
    }

    // Ignore les mots qui ne contiennent pas toutes les lettres de HAYSTACK
    if (preg_match('@[^' . HAYSTACK . ']+@', $word)) {
        continue;
    }

    // Groupe les mots dans des tableaux par longueur (indice)
    // Si le tableau n'est pas défini pour cette longueur on le créé
    if (!isset($words_by_length[$length])) {
        $words_by_length[$length] = [];
    }

    // Ajout du mot au tableau à deux dimensions.
    $words_by_length[$length][] = $word;
}

unset($pool);



// Recherche de longueur complementaire

foreach ($words_by_length as $length => $words) {
    $other_word_length = HAYSTACK_LENGTH - $length;

    if (isset($words_by_length[$other_word_length])) {
        // Pour chaque mot d'une longueur complétementaire, on la compare avec tous les mots
        // qui sont de la longueur restante
        foreach ($words as $word) {
            foreach ($words_by_length[$other_word_length] as $other_word) {
                // On verifie si les deux mots sont un anagramme du mot à tester
                if (str_sort($word . $other_word) === HAYSTACK) {
                    // Affiche le résultat
                    var_dump($word . ' ' . $other_word);
                }
            }
        }
        unset($words_by_length[$length]);
    }
}

